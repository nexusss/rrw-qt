#-------------------------------------------------
#
# Project created by QtCreator 2015-02-17T21:10:11
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = rrw_qt
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    UdpServer.cpp \
    Radar.cpp \
    Client.cpp \
    Udp.cpp \
    RrwProtocol.cpp \
    Processor.cpp \
    kalmanfiltersimple1d.cpp

HEADERS += \
    UdpServer.h \
    Radar.h \
    Client.h \
    Udp.h \
    types.h \
    RrwProtocol.h \
    Processor.h \
    kalmanfiltersimple1d.h
win32 {
  RC_FILE     += ver.rc
  OTHER_FILES += ver.rc
}
