#ifndef CLIENT_H
#define CLIENT_H

#include <Udp.h>
#include<types.h>
#include <RrwProtocol.h>
#include <Processor.h>



/* Этот класс для udp-взаимодействия с клиентом от РЖД
  В нем реализован:
  - сбор спектра от радара
  - цикл обработки запросов от клиента РЖД
  - формирование и отправка ответа на запрос клиенту РЖД

*/
class Client : public Udp
{
    Q_OBJECT

public:
    Client(int port, QString ip);
    status_t status;
    meas_data_t meas_data;
    capture_data_t data;
    unsigned short duration_meas;

    RrwProtocol protocol;
    RrwProtocol protocol_data;

    Processor processor;

    void send_resp(RrwProtocol* prot, QHostAddress sender, int sender_port);

signals:

public slots:
    void connect_udp() {
        //udp->bind(QHostAddress("192.168.10.10"),port, QAbstractSocket::ShareAddress);
        connect(udp, SIGNAL(readyRead()),this, SLOT(readPendingDatagrams()));
    }
    void readPendingDatagrams();
    void prepare_data(capture_data_t data);
    void measureUnsuccessSlot();

};

#endif // CLIENT_H
