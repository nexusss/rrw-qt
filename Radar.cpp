#include "Radar.h"

#include <QFileInfo>
#include <qmath.h>

Radar::Radar(int port, QString ip):Udp(port, ip),numberOfPacket(16),dataSize(512 * 4) {

    sweep += 0xF;
    sweep += 0xC;

    sweep2 += 0xB;
    sweep2 += 0xE;

    curPktNum = 0;
    curSweep = 0;


    for (int i = 0; i < sweep.size(); i++) {
        vector<unsigned short> values(dataSize, 0);

        data.push_back(values);
        //data[i].fill(0);
        //packets_t packets(numberOfPacket);
        //sweeps.push_back(packets);
   }

}


/**Extract sweep direction mark from packet's header*/
inline char sweep_dir(char *pkt) {
  return (unsigned char)pkt[0] & 0x0F;
}

/**Extract ordinal number from packet's header */
inline char pkt_no(char *pkt) {
  return (unsigned char)pkt[0] >> 4;
}

///**Extract and calculate exponent from packet's header */
//inline int pkt_exp(unsigned char *pkt) {
//  return 5 - (pkt[1] - 243);
//}

inline int exp(QByteArray pkt)
{
    QByteArray tmp;
    tmp.push_back(pkt.toHex()[2]);
    tmp.push_back(pkt.toHex()[3]);
    return tmp.toInt(0,16) - 243;
}

void Radar::readPendingDatagrams()
{
  /*  for(int i = 0; i < sweep.size(); i++)
        if(this->data[i].size() != dataSize)
            this->data[i].resize(dataSize);*/

    unsigned short *data;

    while (udp->hasPendingDatagrams()){

         pkt.resize(udp->pendingDatagramSize());
         udp->readDatagram(pkt.data(), pkt.size());
         udpPktNum = pkt_no(pkt.data());

         udpSweep = sweep_dir(pkt.data());

         int ex = exp(pkt);

         if(udpPktNum == curPktNum && (int)udpSweep == (int)sweep[curSweep] || (int)udpSweep == (int)sweep2[curSweep]){

             data = (unsigned short*)pkt.data();
             data += 1;


             for(int i = 0; i < 256; i++){
                 int buf = data[i];                 
                 this->data[curSweep][i + curPktNum * 256] = (int)((buf << 5) >> ex);
//                 this->data[curSweep][i + curPktNum * 256] =  (double)buf / (double)qPow(2,ex);
                 if((int)udpSweep == (int)sweep2[curSweep]){
                     this->data[curSweep][i + curPktNum * 256] *= 8;
                 }

             }


             curPktNum++;
             if(curPktNum == numberOfPacket / sweep.size()){
                 curPktNum = 0;
                 curSweep++;
                    if(curSweep == sweep.size()){ //если достигли придела по свипам, то начинаем заного
                        curSweep = 0;
                        emit data_ready(this->data);

                        dataTimer->start();
                    }

             }
         }
         else{
             curPktNum = 0;
             curSweep = 0;
         }

    }
}
/*
bool Radar::is_packets_collected()
{
    for (int sw = 0; sw < 2; sw++) {
        for (int pkt_num = 0; pkt_num < numberOfPacket; pkt_num++) {

            if (sweeps[sw][pkt_num].size() != 514) {
               // qDebug() << "sweeps[" << sw <<"][" << pkt_num << "].size() = " << sweeps[sw][pkt_num].size();
                return false;
            }
        }
    }
    //qDebug() << "collected";
    return true;
}
*/
/***********************
 *
 *
 *

int number_line_rrw_log = 0;
QString f_up = QString("1.csv");
QString f_down = QString("2.csv");
void Radar::read_sweeps()
{
    clear_data();
    QStringList row[2];
    QFile f1(f_up);
    QFile f2(f_down);
    f1.open(QIODevice::ReadOnly);
    f2.open(QIODevice::ReadOnly);
    QTextStream s1(&f1), s2(&f2);
    int i = 0;
    qDebug() << "number_line_rrw_log " << number_line_rrw_log ;
    while (!f1.atEnd()){
        QString str1=s1.readLine(); // reads line from file
        QString str2=s2.readLine(); // reads line from file
        if (i == number_line_rrw_log) {
            row[0] = str1.split(";");
            row[1] = str2.split(";");
            qDebug() << "num1 " << row[0][0].toInt() << "; num2 " << row[1][0].toInt();
            for (int j = 2, n = 0; j < row[0].size(); j += 2, n++) {
                data[0][ n] = row[0][j].toInt()/1000;
                data[1][ n] = row[1][j].toInt()/1000;
            }
            //qDebug() << "data ready " << number_line_rrw_log << endl;
            break;
        }
        i++;
    }
    if (i < number_line_rrw_log)
        number_line_rrw_log = 0;
    f1.close();
    f2.close();
    number_line_rrw_log++;

    emit data_ready(data);

}*/

/*
 *
 * Это рабочая функция*/
/*
void Radar::read_sweeps()
{
    //cout << "packets is collected\nStart write in DATA\n" << endl;
    clear_data();
    int buf;
    ch2ush_t ch2ush;
    for (int i = 0; i < 2; i++) {
        int n = 0;
        for (int j = 0; j < numberOfPacket; j++) {
            ch2ush.chars[0] = sweeps[i][j][0];
            ch2ush.chars[1] = sweeps[i][j][1];
            int exp_val = ch2ush.chars[1] - 243; //if (j==0)cout << "exp_val = " << exp_val << " ";


            for (int k = 2; k < 514; k += 2, n++) {
                ch2ush.chars[0] = sweeps[i][j][k];
                ch2ush.chars[1] = sweeps[i][j][k + 1];
                buf = ch2ush.value;
                data[i][ n] = (buf << 5) >> exp_val;
            }
        }
    }
    //qDebug() << "data ready";
    emit data_ready(data);
}



void Radar::clear_data()
{
    //cout << "clear\n" << endl;
    if(!data.empty()) {
        for (int i = 0; i < 2; i++) {
            data[i].assign(dataSize, 0);

            for (int j = 0; j < numberOfPacket; j++)
                sweeps[i][j].clear();
        }
    }
}
*/
