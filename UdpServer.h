#ifndef UDPSERVER_H
#define UDPSERVER_H

#include <QObject>
#include <QSettings>
#include <QFileInfo>
#include <Radar.h>
#include <Client.h>

#define FILE_CFG "rrwservers.ini"
/* Класс Мультисервера
  В нем создаются объекты udp-взаимодействия "радар" и "клиент" и
  устанавливается сигнально-слотовая связь между ними

*/
class UdpServer : public QObject
{
    Q_OBJECT

    QVector<Radar*> radars;
    QVector<Client*> clients;

    //QThread thread;
    //QThread thread_client;

public:
    explicit UdpServer(QObject *parent = 0);
    ~UdpServer();
private:
    void start(void);

signals:

public slots:

};

#endif // UDPSERVER_H
