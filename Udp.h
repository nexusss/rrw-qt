#ifndef UDP_H
#define UDP_H

#include <QObject>
#include <QUdpSocket>
#include <QtConcurrent/QtConcurrentRun>
#include <QTimer>

class Udp : public QObject
{
    Q_OBJECT
public:
    int port;
    QString ip;
    QByteArray pkt;
    QThread thread;
    QUdpSocket *udp;
    QTimer *dataTimer;

    explicit Udp(QObject *parent = 0);
    Udp(int port, QString ip = "192.168.10.10");
    ~Udp();

    void start(void);

signals:
    void measureUnsuccessSignal();

public slots:
    void start_socket(void);
    void readPendingDatagrams();
};

#endif // UDP_H
