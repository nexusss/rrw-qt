#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QDebug>
#include <types.h>
#include <vector>
#include "kalmanfiltersimple1d.h"
#include <QFile>
#include <QTextStream>

using namespace std;

#define THRESHOLD_FILE "thresholds.cfg"

#define ARIA_SEARCH_MAX 150 //зона поиска 2 максимума
/* Этот класс для обработки спектров с целью получения
 * параметров обнаруженных целей
*/
class Processor
{
    public:

        Processor();
        ~Processor();
        //bool is_above_threshold (point_t point);
        void get_targets(vector<target_t> *targets_proto, capture_data_t *capture_data);
        void threshold_read(vector<int> *th);
        void threshold_write(vector<int> *th);

    private:
        QVector<KalmanFilterSimple1D *> kalmanFilter;
        const int dataSize;
        /*QFile logFile;
        QTextStream out;*/

    protected:
        int searchAreaMin,searchAreaMax;//зона обработки(в гармониках)

        vector<int> aria;
        vector<int> threshold;




        ItrSweep_t itr_sw_begin[2];
        ItrSweep_t itr_sw_end[2];
        float noise[2];



        capture_data_t* capture_data;

        rrw_targets_t targets;

        void search_targets_recursion(int harm_from, int harm_to);
        bool search_init();
        int max_in_sweep(int harm_from, int harm_to, int sweep);
        float estimateTargetPosition(int harm, int sweep);
        void create_target(int* max_harms);

        //double findCenterOfMass(QVector <double> peaks, QVector<double> radius);
};

#endif // PROCESSOR_H
