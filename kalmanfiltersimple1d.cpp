#include "kalmanfiltersimple1d.h"
#include <QDebug>

KalmanFilterSimple1D::KalmanFilterSimple1D(double q, double r, double f, double h):Q(q),R(r),F(f),H(h)
{
    state = 0;
    covariance = 0;
}

void KalmanFilterSimple1D::SetState(double state, double covariance){
    this->state = state;
    this->covariance = covariance;
}

double KalmanFilterSimple1D::getState(){
    return state;
}

void KalmanFilterSimple1D::Correct(double data){
            //time update - prediction

    if(state == 0 && covariance == 0){
        SetState(data,0.1);
        return;
    }


    X0 = F*state;
    P0 = F*covariance*F + Q;

    //measurement update - correction
    double K = H*P0/(H*P0*H + R);
    state = X0 + K*(data - H*X0);
    covariance = (1 - K*H)*P0;    
}

KalmanFilterSimple1D::~KalmanFilterSimple1D()
{

}

