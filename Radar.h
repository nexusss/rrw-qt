#ifndef RADAR_H
#define RADAR_H

#include <Udp.h>
#include<types.h>
#include <QTimer>
/* Этот класс для udp-взаимодействия с радаром
  В нем реализована аккумуляция пакетов радара и передача спектра клиенту
*/
class Radar : public Udp
{
    Q_OBJECT


    //packet_t packet;
    //sweeps_t sweeps;

    capture_data_t data;

    unsigned int udpPktNum;
    unsigned int udpSweep;
    unsigned int curPktNum;
    unsigned int curSweep;
    QByteArray sweep;
    QByteArray sweep2;

    const unsigned int dataSize;
    const unsigned int numberOfPacket;


private:
    int collect_packets();
    bool is_packets_collected();
    void read_sweeps();

    void clear_data();
public:
    Radar(int port, QString ip);


signals:
    void data_ready(capture_data_t data);


public slots:
    void readPendingDatagrams();

private slots:
};

#endif // RADAR_H
