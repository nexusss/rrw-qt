#include "Client.h"
#include<math.h>

double dbm(double val, int harm) {
    double rval = -90;
    if (val <= 0 || harm <= 0)
        return rval;
    /*
    double dist = harm * 0.5;
    double part1 = 20 * log10(val/1300) - 40.0;
    double part2 = dist <= 200 ? 14.75 * log10(dist) + 15 : 44.0;
    double part3 = -27.32;
    double res = part1 + part2 + part3;
    */

    //double res = 20.0 * log10(val) + (harm <= 400 ? 14.8 * log10(double(harm+1)) - 120 : -86);
    double res = 20.0 * log10(val) + (harm <= 400 ? 14.8 * log10(double(harm+1)) - 120 : -81.5);

    return res < rval ? rval: res;


    //return 20.0 * log10(val/1300.0) - 40.0 + 3.2175 * log10(double(harm) / 0.5) - 3.6; //Chernovs version

}

Client::Client(int port, QString ip):Udp(port, ip)
{
}


void Client::readPendingDatagrams()
{
    disconnect(udp, SIGNAL(readyRead()),this, SLOT(readPendingDatagrams()));

    pkt.resize(udp->pendingDatagramSize());
    //udp->readDatagram(pkt.data(), pkt.size());
    QHostAddress sender;
    quint16 senderPort;
    udp->readDatagram(pkt.data(),512,&sender,&senderPort);


    if (pkt[0] != 0x5A) {
        QTimer::singleShot(1, this, SLOT(connect_udp()));
        //request.clear();
        //qDebug() << "SERVER: request is not valid" << endl;
        return;
    }

    //qDebug() << "recieved: " << pkt.toHex();




    int func_ID = pkt[2];
    unsigned char req_num = pkt[1];
    #ifdef LOG
        qDebug() << "CLIENT: request #" << int(req_num) << ": ";
    #endif

    RrwProtocol* proto = (func_ID == RRW_FN_DATA_ALT)? &protocol_data: &protocol;
    //const request_t req = pkt.data();
    proto->load_request(pkt);

    vector<int> threshold;
    switch(func_ID) {
        case RRW_FN_STATUS:
            #ifdef LOG
                qDebug() << "STATUS" << endl;
            #endif // LOG
            if(status._unused) {//неиспльзуемое поле применяется для определения момента снятия флага наличия данных
                status.has_unread = 0;
                status._unused = 0;

                //meas_data.data_sweeps.clear();
                //meas_data.elapsed = 0;
                //meas_data.targets.clear();
            }
            proto->create_response(&status);
            //qDebug() << "status: has unread = " << (int)status.has_unread << endl;
            //qDebug() << "status: has meas mode = " << (int)status.meas_mode << endl;
            break;

        case RRW_FN_DATA_ALT:
            #ifdef LOG
                qDebug() << "DATA" << endl;
            #endif // LOG
            proto->create_response(&meas_data);
            status._unused = 1;
            break;

        case RRW_FN_MEAS_CTL:
            #ifdef LOG
                qDebug() << "MEAS CTL" << endl;
            #endif // LOG
            //Парсим режим и длительность из запроса клиента
            duration_meas = proto->parse_req_duration();
            status.meas_mode = proto->parse_req_meas_mode();
            #ifdef LOG
                qDebug() << "change MEAS mode to " << (status.meas_mode? "ON":"OFF") << endl;
            #endif // LOG
            proto->create_response(&status, true);
            break;

        case RRW_FN_GET_TH:
        case RRW_FN_SET_TH:
            /*
            if (func_ID == RRW_FN_GET_TH)
                processor.threshold_read(&threshold);
            else {
                //qDebug() << "function 'set threshold' is off" << endl;
                proto->parse_req_threshold(&threshold);
                processor.threshold_write(&threshold);
            }
            */
            qDebug() << "function 'set and get threshold' is off" << endl;
            proto->create_response(&threshold);
            break;

        default:
            proto = 0;
            qDebug() << "UNKNOWN REQ." << endl;
            break;
    }
    if (proto)
        send_resp(proto,sender, senderPort);

}

void Client::send_resp(RrwProtocol* prot, QHostAddress sender, int sender_port) {
    if (prot == 0)
        return;
    const resp_t *resp = prot->get_response();

    if (!resp->empty()) {
        int fn = resp->at(2);
        string msg = "status";

        switch (fn) {
        case 1:
            char buf[20];
            sprintf(buf, "data # %d", (int)meas_data.meas_index);
            msg = buf;
            break;

        case 2:
            msg = "meas ctl";
            break;

        case 3:
            msg = "get th";
            break;

        case 4:
            msg = "set th";
            break;
        }
#ifdef LOG
        qDebug() << "SERVER: send resp ... ";
#endif // LOG
        string res = "Error";



        int sz = udp->writeDatagram((char*) resp->data(), resp->size(),sender, sender_port);

        //qDebug() << "response: ";
        //for (int h = 0; h < resp->size(); h++)
        //    qDebug() << int(resp->at(h));

        //qDebug() << "server send " << sz << " bytes";
        QTimer::singleShot(5, this, SLOT(connect_udp()));


    }
#ifdef LOG
    if (status.meas_mode)
        qDebug() << "SERVER: continue measuring...." << endl;
    else
        qDebug() << "is waiting for client request....";
#endif // LOG
}

void Client::prepare_data(capture_data_t data)
{
    status.last_unsucc = 0;//измерения успешны

    this->data = data;

    meas_data.data_sweeps.clear();
    meas_data.elapsed = 0;
    meas_data.targets.clear();

    vector<target_t> targets;

    processor.get_targets(&targets, &this->data);

    for (int i = 0; i < data.size(); i++) {
        vector<raw_pt_t> vect;
        meas_data.data_sweeps.push_back(vect);        
        for (int j = 0; j < 1024; j++) {//data[i].size()
            raw_pt_t pt;
            //double pw = dbm(data[i][j], j)*1e3;//раскомментировать если нужно передавать Дбм, а следующую строку закомментировать
            double pw = data[i][j];
            pt.power = pw;
            pt.status = j > 0 ? 0 : 3;
            meas_data.data_sweeps[i].push_back(pt);
        }
    }
    meas_data.meas_index++;

    //Если возникнет необходимость передавать продолжительность измерения
    // то его нужно записать в meas_data.elapsed
    //unsigned short elapsed = 33;//timer.elapsed_ms();
    //unsigned short elapsed_add = elapsed >= 90? 0: 90 - elapsed;

    unsigned short elapsed = 33;//timer.elapsed_ms();
    unsigned short elapsed_add = elapsed >= 90? 0: 90 - elapsed;
    meas_data.targets = targets;
    status.has_unread = 1;
    //qDebug() << "prepare OK";
}

void Client::measureUnsuccessSlot(){
    status.last_unsucc = 1;//измерения не успешны
}

/*
void Client::capture_data_2_file(capture_data_t* data) {
    QString f_name = QString("log_port_%1").arg(port);
    QFile f_log(f_name);
    if (f_log.open(QIODevice::Text|QIODevice::Append)) {
        QTextStream out(&f_log);
        //out << time_log << ";" << table_status[1][0] << ";" << table_status[1][1];

        for (int i = 0; i < 2; i++) {
             for (int j = 0; j < (*data)[i].size(); j++)
                 f_data << (*data)[i][j] << ";";
             f_data << endl;
        }
        out << endl;
        f_log.close();
    }


    QFile f("1.dat");
        QByteArray data;
        data.resize(1);
    if (!f.exists()) {
        qDebug() << "not exists" << endl;
        return 1;
    }
        if(f.open(QIODevice::ReadOnly)){
            qDebug() << "start" << endl;
            int i = 0;
            while (true) {
                f.read(data.data(), 1);
                qDebug() << data.toInt();
                QThread::msleep(100);
                i++;
            }
        } else
            qDebug() << "oops" << endl;
    return 1;
   ofstream f_data("data.csv");
   count_sweep++;
   if (count_sweep > 2000)
        count_sweep = 0;
   for (int i = 0; i < 2; i++) {
        f_data << count_sweep << ";";
        for (int j = 0; j < (*data)[i].size(); j++)
            f_data << (*data)[i][j] << ";";
        f_data << endl;
   }
   f_data.close();
}

unsigned long t = 0;
void Client::target_log(target_t trg) {
    if (t == 0) {
        timer_debug.start();
        t = 1;
    }
    ofstream f_data("track.csv",ios::app);
    cout << timer_debug.elapsed_ms() << ";" << trg.dist << ";" << trg.spd << endl;
    f_data << timer_debug.elapsed_ms() << ";" << trg.dist << ";" << trg.spd << endl;
    f_data.close();
}
*/
