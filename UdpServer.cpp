#include "UdpServer.h"

UdpServer::UdpServer(QObject *parent) :
    QObject(parent)
{
    qRegisterMetaType<capture_data_t>("capture_data_t");

    QSettings *cnf = new QSettings(FILE_CFG, QSettings::IniFormat);
    QFileInfo fi(FILE_CFG);
    if(fi.exists()) {
        QString ip = cnf->value("ip").toString();
        for (int port_r = 60; port_r < 80; port_r++) {
            QString key = QString("radar_%1").arg(port_r);
            if (cnf->contains(key)) {
                int port = cnf->value(key).toInt();
                qDebug() << port_r << " -> " << port ;
                radars.push_back(new Radar(port_r, ip));
                clients.push_back(new Client(port, ip));
                //QThread::sleep(1);
            }

        }
    }
    delete cnf;
    if (radars.size() > 0)
        start();
}

void UdpServer::start() {

    for (int i = 0; i < radars.size(); i++) {
        connect(radars[i], SIGNAL(data_ready(capture_data_t)), clients[i], SLOT(prepare_data(capture_data_t)));
        connect(radars[i], SIGNAL(measureUnsuccessSignal()), clients[i], SLOT(measureUnsuccessSlot()));
        radars[i]->start();
        clients[i]->start();
    }

    while(true){
        QThread::sleep(1);
    }
}

UdpServer::~UdpServer() {
    for (int i = 0; i < radars.size(); i++) {
        qDebug() << "delete all";
        delete radars[i];
        delete clients[i];
    }
}
