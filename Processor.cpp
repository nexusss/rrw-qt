#include "Processor.h"
#include<math.h>
#include<fstream>

#include <numeric>
#include<algorithm>
#include<iomanip>

#include <QThread>
#include <QSettings>

unsigned long t_offset = 0;

Processor::Processor():dataSize(2048)
{
    QSettings *cnf = new QSettings("proc.ini", QSettings::IniFormat);

    //signal_noise = 9;
    aria.push_back(0);
    aria.push_back(500);
    searchAreaMin = cnf->value("min",0).toInt();
    searchAreaMax = cnf->value("max",dataSize).toInt();//460;//

    cnf->setValue("min",searchAreaMin);
    cnf->setValue("max",searchAreaMax);

    /*logFile.setFileName("log");
       if (logFile.open(QFile::WriteOnly)) {
           out.setDevice(&logFile);
           // writes "Result: 3.14      2.7       "
       }*/
}

Processor::~Processor()
{
    for(int i = 0;i < kalmanFilter.size(); i++)
        delete kalmanFilter[i];

    kalmanFilter.clear();   
    //logFile.close();
}

void Processor::get_targets(vector<target_t> *targets_proto, capture_data_t *capture_data)
{
    this->capture_data = capture_data;

    search_targets_recursion(searchAreaMin, searchAreaMax);

    target_t trg;
    for (uint i = 0; i < targets.size(); i++) {
        trg.dist = targets[i].s;
        trg.spd = targets[i].v;
        trg.spd_status = 0;
        targets_proto->push_back(trg);
    }
}

void Processor::threshold_read(vector<int> *th) {
    if (!th)
        return;
    th->clear();
    ifstream f(THRESHOLD_FILE);
    if (!f) { //����� ��� - ����� ��������� � ����������� ��������� 0
        ofstream f1(THRESHOLD_FILE);
        if (!f1) {
            qDebug() << "ERROR CREATE THRESHOLD FILE" << endl;
            return;
        }
        for (int i = 0; i < dataSize; i++)
            f1 << 0 << endl;
        f1.close();
        f.open(THRESHOLD_FILE);
        if (!f) {
            qDebug() << "ERROR READ THRESHOLD FILE" << endl;
            return;
        }
    }
    int val;
    while (!(f >> val).eof()) {
        th->push_back(val);
    }
    f.close();
}


void Processor::threshold_write(vector<int> *th) {
    if (!th)
        return;

    ofstream f(THRESHOLD_FILE);
    if (!f) {
        qDebug() << "ERROR OPEN THRESHOLD FILE" << endl;
        return;
    }

    qDebug() << endl << "SERVER: writing threshold...";
    for (uint i = 0; i < th->size(); i++)
        f << th->at(i) << endl;
    f.close();
    threshold = *th;
    qDebug() << "OK!" << endl;

}


//****************************************
bool Processor::search_init() {
    targets.clear();
    if (capture_data == 0){
        qDebug() << "no data";
        return false;
    }

    const int minSignalNoise = 9;
    const float cutLevel = 0.1;

    for(int i = 0; i < 2; i++) {
        capture_data_t sortData;// = *capture_data;
        sortData.resize(2);
        sortData[i].resize(searchAreaMax - searchAreaMin);
        itr_sw_end[i] = (*capture_data)[i].end() - (dataSize - searchAreaMax);
        itr_sw_begin[i] = (*capture_data)[i].begin() + searchAreaMin;
        qCopy(itr_sw_begin[i],itr_sw_end[i],sortData[i].begin());

        qSort((sortData)[i].begin(),(sortData)[i].end());

        float sum_of_elems = accumulate(sortData[i].begin() + sortData[i].size() * cutLevel,sortData[i].end() - sortData[i].size() * cutLevel,0);//#include <numeric>
        noise[i] = sum_of_elems / (sortData[i].size() * (1 - 2.0 * cutLevel));

        float signalNoise = sortData[i].back() / noise[i]; // ??????????? ?????? ???
        //out << endl << "max" << sortData[i].back() << " " << sum_of_elems << " " << (2.0 * sortData[i].size() * 0.1);
        //out << endl << "noise[" << i << "]" << noise[i] << "signalNoise" << signalNoise;


        if(signalNoise * 0.35 > minSignalNoise)
            noise[i] *= (signalNoise * 0.35);
        else
            noise[i] *= minSignalNoise;

        //out << endl << "noise[" << i << "]" << noise[i] << "signalNoise" << signalNoise;
    }

    /*??????? ?? ????
    for(int i = 0; i < 2; i++) {
        itr_sw_end[i] = (*capture_data)[i].end() - (1024 - searchAreaMax);
        itr_sw_begin[i] = (*capture_data)[i].begin() + searchAreaMin;

        int sum_of_elems = accumulate(itr_sw_begin[i],itr_sw_end[i],0);//#include <numeric>
        noise[i] = sum_of_elems / (searchAreaMax - searchAreaMin);

    }*/
    return true;
}
/*
 * ������-������� �� ������ ����������
 * ����� ������ ���������:
 * ������� ���������� �������� � ������� UP, ����� ���� ���������� �������� � ������� DOWN
 * � �������� +- 15 ��������. ��� ���� ��������� � ����� �����.
 * ?���� ����� �� ����� ����������� ������� ���������� � ����� ����� ���������� ������� � ������.
 * ����� ���� �� ��� ���, ���� �� ���� ���������� ������� ��������.
 *
 * ��� ������ ����������� ������� ���������� ��� �������, ������� ��������� ���
 * ������� �������� ������� * signal_noise (����� ����� ������������������)
 *
*/
void Processor::search_targets_recursion(int harm_from, int harm_to) {
    if (harm_to - harm_from < 10) {
        //qDebug() << "no max." << endl;
        return;
    }

    if (harm_from == searchAreaMin && harm_to == searchAreaMax)
        if (!search_init())
            return;

    //qDebug() << "search in [" << harm_from << "; " << harm_to << "]: ";
    //ItrSweep_t itr_max[2], itr_from[2], itr_to[2];

    int max_harms[2];
    max_harms[0] = max_in_sweep(harm_from, harm_to, 0);
    if (max_harms[0] < 0)
        return;

    int left_from, left_to, right_from, right_to;

    int h_from_1, h_to_1;

    h_from_1 = (max_harms[0] - harm_from > ARIA_SEARCH_MAX) ? max_harms[0] - ARIA_SEARCH_MAX: harm_from;
    h_to_1 = (harm_to - max_harms[0] > ARIA_SEARCH_MAX) ? max_harms[0] + ARIA_SEARCH_MAX: harm_to;
    max_harms[1] = max_in_sweep(h_from_1, h_to_1, 1);

    if (max_harms[1] < 0) {//no max in sweep 1
        right_from = max_harms[0] + 2;
        left_to = max_harms[0] - 2;
        //qDebug() << "no target";
    } else {//target is detected
        //****//
        //������������ ��������� ����
        /*
        if (harm_from == searchAreaMin && harm_to == searchAreaMax) {
            float max_P0 = (*capture_data)[0][max_harms[0]];
            float max_P1 = (*capture_data)[1][max_harms[1]];
            if (max_P0 < noise[0] * signal_noise)
                return;
            float k = max_P0 / noise[0];

            if(max_P0 * 0.3 > noise[0] * signal_noise)
                noise[0] = max_P0 * 0.3;
            if(max_P1 * 0.3 > noise[1] * signal_noise)
                noise[1] = max_P1 * 0.3;
        }*/

        //create_target
        create_target(&max_harms[0]);



        //qDebug() << (*capture_data)[0][max_harms[0]] << ", " << (*capture_data)[1][max_harms[1]] << " noise =[" << noise[0] << "; " << noise[1] << "]";
        //****//
        int ofst_search = 5;
        if (max_harms[0] >= max_harms[1]) {
            right_from = max_harms[0] + ofst_search;
            left_to = max_harms[1] - ofst_search;
        }
        else {
            right_from = max_harms[1] + ofst_search;
            left_to = max_harms[0] - ofst_search;
        }
    }

    //return;

    right_to = harm_to;
    search_targets_recursion(right_from, right_to);

    left_from = harm_from;
    search_targets_recursion(left_from, left_to);
}

int Processor::max_in_sweep(int harm_from, int harm_to, int sweep) {
    ItrSweep_t from, to, first, max_itr;
    //first = itr_sw_begin[sweep];
    first = (*capture_data)[sweep].begin();
    //last = itr_sw_end[sweep];
    from = first + harm_from;
    to = first + harm_to + 1;

    max_itr = max_element(from, to);

    /*if (*max_itr <= noise[sweep] * signal_noise)
        return -1;*/

    if (*max_itr <= noise[sweep])
            return -1;

    if(max_itr - first < 1 || max_itr - first > dataSize - 1)
        return -1;

    float peak = *max_itr;
    float prevPeak = *(max_itr - 1);
    float nextPeak = *(max_itr + 1);

    if(peak < nextPeak || peak < prevPeak)
        return -1;

    return max_itr - first;
}

void Processor::create_target(int* max_harms) {
    rrw_target_t target;
    float estim_peaks[2];
    for (int i = 0; i < 2; i++) {
        target.max_harms[i] = max_harms[i];
        //out << endl << "max_harms[i" << i << "] " << max_harms[i] << endl;
        estim_peaks[i] = estimateTargetPosition(max_harms[i], i) - 0.5;
        if(estim_peaks[i] < 0)
            estim_peaks[i] = 0;
        target.max_harms_f[i] = estim_peaks[i];
    }
    double correctSpeed = 0;

    int numTarget = targets.size();
    if(numTarget == kalmanFilter.size()){
        kalmanFilter.push_back( new KalmanFilterSimple1D(2.0,15.0));        
    }


    //qDebug() << "estim_peaks[1]" << estim_peaks[1] << "estim_peaks[0]" << estim_peaks[0] << "(estim_peaks[1] - estim_peaks[0]) * 0.5 * 0.7" << (estim_peaks[1] - estim_peaks[0]) * 0.5 * 0.7;
    kalmanFilter[numTarget]->Correct((estim_peaks[1] - estim_peaks[0]) * 0.5 * 0.7);//��������� ������ �������� ��� ��������������� ����
    correctSpeed = kalmanFilter[numTarget]->getState();

    //qDebug() << "numTarget" << numTarget << "speed" << correctSpeed << kalmanFilter.size();

    target.v = correctSpeed;//1.4; //km/h
    target.s = (estim_peaks[1] + estim_peaks[0]) * 0.5 * 0.5 ; // m

    /*out <<  endl << "targets.s" << target.s << "targets.v" << target.v << endl
         << (estim_peaks[1] + estim_peaks[0]) << " " << (estim_peaks[1] + estim_peaks[0]) * 0.5 << " " << (estim_peaks[1] + estim_peaks[0]) * 0.5 * 0.5 << endl
             << "estim_peaks[1]" << estim_peaks[1] << "estim_peaks[0]" << estim_peaks[0];*/
    //qDebug() << "empty" << targets.empty();
    //qDebug() << "-----------------------------------";
    //if(targets.size() > 0)
    //qDebug() << "targets.s" << targets[0].s << "targets.v" << targets[0].v;
    if (target.s <= aria[1] && target.s >= aria[0]) {
        targets.push_back(target);
    }
    else{
        delete kalmanFilter.last();
        kalmanFilter.pop_back();
    }
}

inline float estimDelta(ItrSweep_t peak,ItrSweep_t begin){
    return peak - begin + (*peak - *(peak - 1) ) / (2.0 * *peak - *(peak - 1) - *(peak + 1));
}

//������� ��������� ���������� �� ����(� ����������)
//������� ���������:
//����� ���������, ����� �����
//�������� ���������:
//���������� ���������� �� ����
float Processor::estimateTargetPosition(int harm, int sweep) {
    if (capture_data == 0)
        return 0;

    const int sector = 6;
    QVector <float> maximumArr;//������ ����������
    QVector <float> pos;//������ ��������
    ItrSweep_t begin = (*capture_data)[sweep].begin() + harm - sector;
    ItrSweep_t endharm = (*capture_data)[sweep].begin() + harm + sector;

    //����� ����� ������� ����������, ��� ����� �� ������, ��� � � �����
    for(ItrSweep_t it = begin; it != endharm; it++){//���� � ������� ����� �����, �� ������� ������
        bool maxFind = false;

        while((!maxFind) && (it != endharm)){ //���� �� ������ �������� ��� �� ��������� �����

            float peak = *it;
            float prevPeak = *(it - 1);//��� �� �������� ����� ������� ��������
            float nextPeak = *(it + 1);

            bool anyoneMoreNoise = peak > noise[sweep];//��������� ������ ���� ���� ����
            bool currentPeakIsBigger = (nextPeak < peak && prevPeak < peak);//���������� ���������, ������ �������

            if(anyoneMoreNoise && currentPeakIsBigger){
                maxFind = true;
                maximumArr.push_back(peak);//���������� �������� ����
                float estimHarm = estimDelta(it,(*capture_data)[sweep].begin());//�������� ���������
                pos.push_back(estimHarm);//���������� ���������
            }
            else{
                it++;
            }

        }
        if(it == endharm){
            --it;
        }
    }

    //������ ���� = �����(����� * ������) / �����(����)
    double summMass = 0;
    double summMul = 0;
    for(int i = 0; i < pos.size(); i++){
        //out << endl << "peaks[" << i << "] " << peaks[i] << " rad[" << i << "] " << radius[i];
        summMass += maximumArr[i];
        double mul = maximumArr[i] * pos[i];
        summMul += mul;
    }
    if(summMass != 0)
        return summMul / summMass;
    else
        return harm;
}
