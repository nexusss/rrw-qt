#ifndef KALMANFILTERSIMPLE1D_H
#define KALMANFILTERSIMPLE1D_H


class KalmanFilterSimple1D
{
    double X0;
    double P0;

    double Q;
    double R;
    double F;
    double H;

    double state;
    double covariance;

public:
    KalmanFilterSimple1D(double q, double r, double f = 1, double h = 1);
    void Correct(double data);
    void SetState(double state, double covariance);
    double getState();
    ~KalmanFilterSimple1D();
};

#endif // KALMANFILTERSIMPLE1D_H
