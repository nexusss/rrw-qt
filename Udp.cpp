#include "Udp.h"

Udp::Udp(QObject *parent) :
    QObject(parent)
{
    udp = 0;
}

Udp::Udp(int port, QString ip):port(port),ip(ip) {
    udp = 0;
    dataTimer = 0;
}

Udp::~Udp() {

    delete udp;
}

void Udp::start() {
     moveToThread(&thread);
    //qDebug() << "try to start";
    connect(&thread, SIGNAL(started()), this, SLOT(start_socket()));
    thread.start();
}

void Udp::start_socket(void) {
    if (udp)
        delete udp;
    udp = new QUdpSocket;
    if(!udp->bind(QHostAddress::Any,port, QAbstractSocket::ReuseAddressHint)) {//QHostAddress(ip)
        qDebug() << "udp port " << port << " - error bind";
        udp->close();
    }
    qDebug() << "udp port " << port;
    connect(udp, SIGNAL(readyRead()),this, SLOT(readPendingDatagrams()));

    if(dataTimer)
        delete dataTimer;
    dataTimer = new QTimer;
    dataTimer->setInterval(300);
    connect(dataTimer,SIGNAL(timeout()),this,SIGNAL(measureUnsuccessSignal()));
    dataTimer->start();
    /*
    QByteArray d("1");
    disconnect(udp, SIGNAL(readyRead()),this, SLOT(readPendingDatagrams()));
    qDebug() << udp->writeDatagram(d,QHostAddress::Broadcast, port);
    udp->waitForBytesWritten();
    connect(udp, SIGNAL(readyRead()),this, SLOT(readPendingDatagrams()));
*/

    //qDebug() << "OK";
}

void Udp::readPendingDatagrams() {
    qDebug() << "UDP DATA" << endl;

}

