#ifndef TYPES_H_INCLUDED
#define TYPES_H_INCLUDED

#include<vector>

#define LOG111

using namespace std;

//Function codes
#define RRW_FN_STATUS    0x00
#define RRW_FN_DATA_ALT  0x01
#define RRW_FN_MEAS_CTL  0x02
#define RRW_FN_GET_TH    0x03
#define RRW_FN_SET_TH    0x04

//Function return codes
#define RRW_RV_SUCCESS  0x00
#define RRW_RV_BAD_REQ  0x01
#define RRW_RV_BUSY     0x02
#define RRW_RV_ERROR    0xFF

//Measurement modes
#define RRW_MEAS_ON_REQ 0x00
#define RRW_MEAS_CYCLE 0x01

#define RRW_MAX_TH_PTS_NUM 1024
#define RRW_MAX_TRGTS_NUM 255

#define SWEEP_RISE 0
#define SWEEP_FALL 1

#define DIR_RISE ((char)0xF)
#define DIR_FALL ((char)0xC)

typedef struct {
  unsigned char _;
  unsigned char no;
  unsigned char fn;
} base_req_t;

typedef struct {
  unsigned char _;
  unsigned char no;
  unsigned char fn;
  unsigned char rv;
} base_resp_t;

#pragma pack(push,1)
typedef struct {
  unsigned char fault       : 1;
  unsigned char ready       : 1;
  unsigned char meas_mode   : 1;
  unsigned char last_unsucc : 1;
  unsigned char has_unread  : 1;
  unsigned char _unused     : 3;
} status_t;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct {
  base_resp_t base_resp;
  status_t status;
  unsigned char details;
  signed char t[3];
  unsigned char i[3];
  signed char u[4];
} status_resp_t;
#pragma pack(pop)

typedef struct {
  base_resp_t base_resp;
  status_t status;
} meas_ctl_resp_t;

typedef struct {
  base_resp_t base_resp;
  status_t status;
  unsigned char meas_no;
  unsigned short elapsed;
} data_resp_t;

typedef struct {
	unsigned char det : 1;
	unsigned char dir : 2;
	unsigned char spd_status : 2;
	unsigned char pow_status : 2;
	unsigned char spd_val;
	short pow_val;
} data_el_t;

typedef struct {
  double spd;
  double dist;
  unsigned char spd_status;
} target_t;

#pragma pack(push,1)
typedef struct {
  int power;
  unsigned char status;
} raw_pt_t;
#pragma pack(pop)

typedef struct {
  base_resp_t base_resp;
  unsigned char meas_no;
  unsigned short elapsed;
  raw_pt_t rise[1024];
  raw_pt_t fall[1024];
  unsigned char ntargets;
  target_t *targets;
} data_alt_resp_t;


typedef struct {
  unsigned short p;
  unsigned short d;
} thpt_t;

typedef struct {
  base_resp_t base_resp;
  unsigned short npts;
  thpt_t pts[1024];
} getth_resp_t;

typedef data_el_t target_pt_t;

typedef struct {
  base_req_t base_req;
  unsigned char mode;
  unsigned short cycle_dur;
} meas_ctl_req_t;

typedef struct {
  base_req_t base_req;
  unsigned short npts;
  thpt_t pts[1024];
} setth_req_t;


typedef struct {
    int max_harms[2];
    float max_harms_f[2];
    double s;
    double v;
    double v1;
    float power[2];
} rrw_target_t;

typedef struct {
    int size;
    int count;
    int unit;
    int sweeps;
} pkt_params_t ;

typedef vector<char> packet_t;
typedef vector<packet_t> packets_t;
typedef vector<packets_t> sweeps_t;

typedef vector<rrw_target_t> rrw_targets_t;
typedef vector<unsigned short> point_t;
typedef vector<point_t> points_t;
typedef point_t::iterator ItrSweep_t;

typedef points_t capture_data_t;

typedef vector<unsigned char> resp_t;
typedef vector<unsigned char> request_t;

typedef union {
    int value;
    unsigned char chars[sizeof(int)];
} ch2int_t;

typedef union {
    unsigned short value;
    unsigned char chars[sizeof(unsigned short)];
} ch2ush_t;

typedef union {
    double value;
    unsigned char chars[sizeof(double)];
} ch2d_t;



//typedef vector<target_t> targets_t;
#endif // TYPES_H_INCLUDED
